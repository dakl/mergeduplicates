def phred2score(phred):
	return ord( phred ) - 33
	
def score2phred(score):
	return chr( score + 33 )

class SAMRecord:
	"""A class to hold information from each line in a BAM file"""
	def __init__(self, string):
		vals = string.split( )
		self.QNAME = vals[0]
		self.FLAG  = int(vals[1])
		self.RNAME = vals[2]
		self.POS   = int(vals[3])
		self.MAPQ  = vals[4]
		self.CIGAR = vals[5]
		self.RNEXT = vals[6]
		self.PNEXT = int(vals[7])
		self.TLEN  = int(vals[8])
		self.SEQ   = list( vals[9] )
		self.QUAL  = list( vals[10] )
		self.tags  = vals[11:]

	def hasIndels(self):
		if 'I' in self.CIGAR or 'D' in self.CIGAR:
			return True
		return False

	def getTag(self, tag):
		for t in self.tags:
			if t.startswith(tag):
				x = t.split(":")
				return x[2]

	## function to merged reads
	# merges the current read with 'otherread'
	# maxqual is the maximum quality after merging, commonly set to 50
	# minqual is the minimum quality for a base to be considered, commonly set to 3.
	def mergeWithRead(self, otherread, maxqual, minqual):
		for idx in range(len( self.SEQ ) ): 
			if ( self.QUAL[idx] >= minqual and phred2score(otherread.QUAL[idx]) >= minqual ): # if both bases are over the minimum quality, consider them for merging
				if ( self.SEQ[idx] == otherread.SEQ[idx] ): # if bases are the same, merge them
					mergedqual = phred2score(self.QUAL[idx]) + phred2score(otherread.QUAL[idx])
					if ( mergedqual > maxqual ):
						mergedqual = maxqual
					self.QUAL[idx] = score2phred( mergedqual )
				else: ## if bases mismatch, set to N
					self.SEQ[idx] = 'N'
		

	def mergeReads(self, reads, maxqual, minqual, useBest):
		def most_common(lst):
			return max(set(lst), key=lst.count)
		merged = reads[0]
		for k in range(len(reads[0].SEQ)):
			bases = [ x.SEQ[k] for x in reads ]
			quals = [ phred2score(x.QUAL[k]) for x in reads ]
			most_common_base = most_common(bases)
			idx = [ i for i,x in enumerate(bases) if x == most_common_base ] ## which( INDS == IND ) in python
			if( len(idx) >= 0.75*len(reads) ): ## if at least 75% of the reads support the most common base
				quals_for_most_common_base = [ quals[i] for i in idx ]
				qualsum = max(quals_for_most_common_base)
				if( not useBest ): # if we're not picking the best, but merging (adding BQs)
					for i in range(len(quals_for_most_common_base)):
						if quals_for_most_common_base[i] < minqual:
							quals_for_most_common_base[i] = 0
					qualsum = sum(quals_for_most_common_base)

				if(qualsum) > maxqual:
					qualsum = maxqual
			else: ## if there is not a 75% majority vote, set base to N and qual to 2
				most_common_base = "N"
				qualsum = 2
			merged.SEQ[k] = most_common_base
			merged.QUAL[k] = score2phred(qualsum)
		return merged
		
	def isPaired(self):
		return self.FLAG & 1

	def isProperlyPaired(self):
		return self.FLAG & 2

	def isUnmapped(self):
		return self.FLAG & 4

	def mateIsUnmapped(self):
		return self.FLAG & 8

	def isMappedOnReverseStrand(self):
		return self.FLAG & 16

	def mateIsMappedOnReverseStrand(self):
		return self.FLAG & 32

	def isFirstInPair(self):
		return self.FLAG & 64

	def isSecondInPair(self):
		return self.FLAG & 128

	def isSecondaryAlignment(self):
		return self.FLAG & 256

	def failedQC(self):
		return self.FLAG & 512

	def isPCRDuplicate(self):
		return self.FLAG & 1024

        def setIsPCRDuplicate(self, setAsPCRDup):
                if setAsPCRDup:
			if not self.isPCRDuplicate(): ## only set the flag if it' not set for this read
				self.FLAG += 1024
		else:
			if self.isPCRDuplicate(): ## only remove the flag if it's previously set
				self.FLAG -= 1024

	def getKey(self):
		a = [ self.RNAME ]
		if(self.isPaired() and self.TLEN > 0 ): ## if I'm mapped on the left side in a pair, adj POS for soft clipping#
			a.append( str(self.POS ) ) 
		elif( self.isPaired() and self.TLEN < 0 ): ## if I'm mapped on the right size in a pair, use PNEXT for key to avoid indel troubles
			a.append( str(self.PNEXT ) )
		else:
			a.append( str(self.getCoordAdjustedForSoftClipping() ) )
		a.append( str(self.isMappedOnReverseStrand() ) )
		a.append( str( len(self.SEQ) ) )
		a.append( self.getTag("RG") )
		a.append( str( self.RNEXT ) )
		a.append( str( self.TLEN ) ) ## instead of PNEXT, since PNEXT can vary if the rightmost read is softclipped. TLEN (prev ISIZE) will not.
		a.append( str( self.mateIsMappedOnReverseStrand() ) )
#		a.append( str( self.hasIndels() ) )
		return "_".join(a)

	def getCoordAdjustedForSoftClipping(self):
		import re
		m=re.match('(^\d+)S', self.CIGAR)
		if(m):
			return self.POS - int(m.group(1))
		else:
			return self.POS

	def toString(self):
		a = [ str(self.QNAME) ]
		a.append( str(self.FLAG)   )
		a.append( str(self.RNAME)  )
		a.append( str(self.POS)    )
		a.append( str(self.MAPQ)   )
		a.append( str(self.CIGAR)  )
		a.append( str(self.RNEXT)  )
		a.append( str(self.PNEXT)  )
		a.append( str(self.TLEN)   )
		a.append( "".join(self.SEQ)    )
		a.append( "".join(self.QUAL)   )
		a.append( "\t".join(self.tags) )
		return "\t".join(a) + "\n"
	
	

version = '0.1'
