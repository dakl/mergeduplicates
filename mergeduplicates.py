#!/usr/bin/env python
import optparse
import sys
import subprocess
import re
import pipes
import os
import SAMRecord
from datetime import datetime

##LIBRARY
##UNPAIRED_READS_EXAMINED
##READ_PAIRS_EXAMINED
##UNMAPPED_READS
##UNPAIRED_READ_DUPLICATES
##READ_PAIR_DUPLICATES
##READ_PAIR_OPTICAL_DUPLICATES
##PERCENT_DUPLICATION
##ESTIMATED_LIBRARY_SIZE

def update_metrics(rec):
    global metrics
    global rg
    rgtag = rec.getTag("RG")
    lib = rg[ rgtag ]["LB"]
    if not lib in metrics:
        metrics[lib] = { "UNPAIRED_READS_EXAMINED" : 0, "READ_PAIRS_EXAMINED" : 0, "UNMAPPED_READS" : 0, 
                         "UNPAIRED_READ_DUPLICATES" : 0, "READ_PAIR_DUPLICATES" : 0, "READ_PAIR_OPTICAL_DUPLICATES" : 0,
                         "PERCENT_DUPLICATION" : 0.0, "ESTIMATED_LIBRARY_SIZE" : 0 }
    if rec.isUnmapped():
        metrics[lib]["UNMAPPED_READS"] += 1
    elif (not rec.isPaired()) or rec.mateIsUnmapped():  ## read is single end or mate is unmapped
        metrics[lib]["UNPAIRED_READS_EXAMINED"] += 1
        if rec.isPCRDuplicate():
            metrics[lib]["UNPAIRED_READ_DUPLICATES"] += 1
    else: ## read is PE and both mates are mapped
        if rec.isFirstInPair(): ## only update metrics for reads that are the first in a pair.
            metrics[lib]["READ_PAIRS_EXAMINED"] += 1 
            if rec.isPCRDuplicate():
                metrics[lib]["READ_PAIR_DUPLICATES"] += 1 
        


def write_bam_header_from_file( outfile, infile ): ##take the header from infile, and open a pipe for writing
    global rg
    cmds = ['samtools', 'view','-H',infile]
    proc = subprocess.Popen(cmds, stdout=subprocess.PIPE)
    header = [line for line in iter(proc.stdout.readline,'')]
    t = pipes.Template()
    t.append("samtools view -bS - > $OUT", "-f" )
    f = t.open(outfile, 'w')
    for ln in header:
        f.write(ln)
        if ln.startswith("@RG"):
            elements = ln.split()
            tag = {}
            for e in elements[1:]:
                tagname, value = e.split(":")
                tag[tagname] = value
            rg[ tag["ID"] ] = tag
    return f

def process_bam_file ( input ):
    global reads
    global options
    numberOfReadsChecked = 0
    cmds = ['samtools', 'view', input]
    proc = subprocess.Popen(cmds, stdout=subprocess.PIPE)

    ## from here: http://stackoverflow.com/questions/2804543/read-subprocess-stdout-line-by-line  
    last = None
    lastkey = 0
    lasttime = datetime.now()
    for line in iter(proc.stdout.readline,''):
        rec = SAMRecord.SAMRecord( line )
        if not last:
            last = rec
        currentkey = rec.getKey()
        currentpos = rec.RNAME + "_" + str( rec.getCoordAdjustedForSoftClipping() )
        lastpos    = last.RNAME + "_" + str( last.getCoordAdjustedForSoftClipping() )

        if currentpos != lastpos: ## if the current read comes from a new location
            mergeReads(rec.RNAME, rec.getCoordAdjustedForSoftClipping(), False )
            
        if rec.isUnmapped() and rec.mateIsUnmapped(): # if the read is unmapped
            if rec.QNAME in mergedMates.keys(): ## if the mate has been merged
                if rec.QNAME == mergedMates[rec.QNAME]: ## and has the same name as the merged read
                    outfh.write( rec.toString() ) ## then write the read to file
                else:
                    mergedMates.pop(rec.QNAME )
        else: # if it's mapped
            if not currentkey in reads:
                reads[currentkey] = [ rec ]
            else:
                reads[currentkey].append( rec )
        
        last = rec

        numberOfReadsChecked += 1
        if numberOfReadsChecked % int(options.numberofreadsbetweenreports) == 0:
            currtime = datetime.now()
            timediff = currtime - lasttime
            timediff = timediff.seconds + timediff.microseconds/1E6
            if timediff == 0: ## don't divide by 0
                timediff = 1.0 
            sys.stderr.write("MergeDuplicates: " + str(numberOfReadsChecked) + " reads investigated for duplicates at " + str(datetime.now()) + " (" + 
                             str(round(int(options.numberofreadsbetweenreports)/timediff,2)) + " reads/sec) with " + str(len(mergedMates)) + " read names in memory." + "\n")
            lasttime = currtime
            

    mergeReads(rec.RNAME, rec.getCoordAdjustedForSoftClipping(), True) ## final pass when last read has been fetched from BAM file




# merge reads in list and remove them from 'reads'

def mergeReads(currentChr, currentPos, forceMerge):
    global reads
    global outfh
    global mergedMates
    global options
    global metrics
    global mates_to_skip

    ## if any PE reads span the exact length of a SE reads, skip the PE
    ## =========== merged SE read
    ## ===     === PE pair, skip these
    def dup(rd):
        mates_to_skip[ rd.QNAME ] = rd.QNAME ## add this to reads to skip
        rd.setIsPCRDuplicate(True)
        update_metrics(rd)

#    for key in reads.keys():
#        r = reads[key][0]
#        
#        if r.RNAME == currentChr and r.getCoordAdjustedForSoftClipping() + 153 > currentPos and not forceMerge:
#            continue
#        
#        if r.isPaired():
#            # look for orphan mates     
#            should_skip_reads = False
#            for rd in reads[key]:
#                if rd.QNAME in mates_to_skip:
#                    dup(rd)
#                    should_skip_reads = True
#
#            if should_skip_reads: ## remove key if the mate has already been removed
#                reads.pop( key )
#
#            else:
#                readsToCheck = [ reads[k][0] for k in reads.keys() ] ## compare r first read of all other groups starting at current pos
#                for rtc in readsToCheck:
#                    if r.TLEN == len( rtc.SEQ ) and not rtc.isPaired(): ## if an SE read spans the exact size of the PE pair, ignoring indels...
#                        for rd in reads[key]: ## set all PE reads as duplicates
#                            dup( rd )
#                        reads.pop( key )
#                        
#                        break # no need to go through rest of 'readsToCheck'



    for key in reads.keys():
        readsToMerge = reads.get(key)
        ## check if this list of reads should be merged, or wait until more reads are checked
        r = readsToMerge[0]
      
        if r.RNAME == currentChr and r.getCoordAdjustedForSoftClipping() + 153 > currentPos and not forceMerge:
            continue

        ## if some, but not all reads have indels, remove them from the reads to be merged.
        ## if all reads have indels, merge them
        num_reads_w_indels = sum([x.hasIndels() for x in readsToMerge ])
        if( len(readsToMerge) > num_reads_w_indels ): ## if we have reads w/o indels
            tmp = []
            for r in readsToMerge:
                if not r.hasIndels():
                    tmp.append(r)
            readsToMerge = tmp
        merged = readsToMerge[0]
        update_metrics(merged)
        for read in readsToMerge[1:]: 
            read.setIsPCRDuplicate(True)
            update_metrics(read)
            #merged.mergeWithRead( read, options.maxqual, options.minqual ) 
            if merged.isPaired(): ## if PE reads
                if read.QNAME in mergedMates: ## if mates has been merged previously
                    merged.QNAME = mergedMates.pop( read.QNAME )
                    if merged.QNAME in mergedMates :
                        mergedMates.pop( merged.QNAME )
                else: ## if mates are unseen, add then to the dict of merged reads
                    mergedMates[ read.QNAME ]   = merged.QNAME
                    mergedMates[ merged.QNAME ] = merged.QNAME
        merged = merged.mergeReads(readsToMerge, options.maxqual, options.minqual, options.usebest )
        reads.pop(key) ## since we merged, remove the reads from the stack
        outfh.write( merged.toString() )
        del readsToMerge
        #reads[key] = merged
        del key
        del merged


def write_metrics():
    global metrics
    mf = open(options.metrics, "w")
    mf.write("## METRICS CLASS\tse.ki.meb.klevebring.mergeduplicates\n")
    mf.write("LIBRARY\tUNPAIRED_READS_EXAMINED\tREAD_PAIRS_EXAMINED\tUNMAPPED_READS\tUNPAIRED_READ_DUPLICATES\tREAD_PAIR_DUPLICATES\tREAD_PAIR_OPTICAL_DUPLICATES\tPERCENT_DUPLICATION\tESTIMATED_LIBRARY_SIZE\n")
    for lib in metrics:
        pctdup = ( 0.0 + metrics[lib]['READ_PAIR_DUPLICATES'] + metrics[lib]['UNPAIRED_READ_DUPLICATES'] ) / ( 0.0 + metrics[lib]['READ_PAIRS_EXAMINED'] + metrics[lib]['UNPAIRED_READS_EXAMINED'] )
        metrics[lib]["PERCENT_DUPLICATION"] = pctdup
        mf.write(str(lib) + "\t")
        mf.write(str(metrics[lib]['UNPAIRED_READS_EXAMINED'] ) + "\t")
        mf.write(str(metrics[lib]['READ_PAIRS_EXAMINED']     ) + "\t")
        mf.write(str(metrics[lib]['UNMAPPED_READS']          ) + "\t")
        mf.write(str(metrics[lib]['UNPAIRED_READ_DUPLICATES']) + "\t")
        mf.write(str(metrics[lib]['READ_PAIR_DUPLICATES']    ) + "\t")
        mf.write(str(metrics[lib]['READ_PAIR_OPTICAL_DUPLICATES'])  + "\t")
        mf.write(str(metrics[lib]['PERCENT_DUPLICATION']     ) + "\t")
        mf.write(str(metrics[lib]['ESTIMATED_LIBRARY_SIZE']  ) + "\n")



def main():
	global reads # dict of arrays, dict KEY=RNAMEPOS, holds single end reads
					 # fragments[ 'chr1_1234' ] = [SAMRecord1, SAMRecord2, SAMRecord3]
	global mergedMates # holds info on the merged name for each read that has been merged
	global mates_to_skip # hold mates that should be marked as dups
	global outfh
	global options
	global metrics
	global rg

	p = optparse.OptionParser()
	p.add_option('--input', '-i', action="store", help="Input BAM file to split")
	p.add_option('--output', '-o', action="store", help="Output for BAM file")
	p.add_option('--metrics', '-x', action="store", help="Output for metrics")
	p.add_option('--maxqual', '-M', action="store", default=50, help="Maximum BQ after merging")
	p.add_option('--minqual', '-m', action="store", default=3, help="Minimum BQ to cosider base for merging (do not change unless you know what you are doing)")
	p.add_option('--use-best-qual', '-B', action="store_true", dest="usebest", default=False, help="If set, picks the highest base quality instead of adding the base qualities when merging duplicates")
	p.add_option('--numberofreadsbetweenreports', '-n', action="store", default=1000, help="Number of reads between progress reports")

	options, arguments = p.parse_args()	
	
	if not options.input or not options.output or not options.metrics:
		p.print_help()
		sys.exit(1)
	
	if not os.path.exists( options.input ):
		print "ERROR: " + options.input + " does not exist"
		p.print_help()
		sys.exit(2)
	
        options.minqual = int( options.minqual )
        options.maxqual = int( options.maxqual )

	reads = {}
	mergedMates = {}
        mates_to_skip = {}
        metrics = {}
        rg = {}
	
	outfh = write_bam_header_from_file( options.output, options.input )
	sys.stderr.write("MergeDuplicates: Starting to look for duplicates at " + str(datetime.now()) + "\n")
	process_bam_file( options.input )
	outfh.close()

	print "Number of unmatched reads: " + str( len( mergedMates ) )

        write_metrics()

if __name__ == '__main__':
    main()
		

# 	f.write("HISEQ2:181:C166AACXX:5:1204:6771:80551	0	chr1	1159241	37	5M	*	0	0	TGTGA	!\"#$%	RG:Z:1AAXX.1	XT:A:U	NM:i:0	X0:i:1	X1:i:0	XM:i:0	XO:i:0	XG:i:0	MD:Z:83\n")
# 	f.write("HISEQ2:181:C166AACXX:5:1204:6771:80552	0	chr1	1159241	37	5M	*	0	0	TGTGA	!\"#$%	RG:Z:1AAXX.1	XT:A:U	NM:i:0	X0:i:1	X1:i:0	XM:i:0	XO:i:0	XG:i:0	MD:Z:83\n")
# 	f.write("HISEQ2:181:C166AACXX:5:1204:6771:80553	0	chr1	1159241	37	5M	*	0	0	TGTGA	!\"#$%	RG:Z:1AAXX.1	XT:A:U	NM:i:0	X0:i:1	X1:i:0	XM:i:0	XO:i:0	XG:i:0	MD:Z:83\n")
# 	f.write("HISEQ2:181:C166AACXX:5:1204:6771:80554	0	chr1	1159241	37	5M	*	0	0	TGTGA	!\"#$%	RG:Z:1AAXX.1	XT:A:U	NM:i:0	X0:i:1	X1:i:0	XM:i:0	XO:i:0	XG:i:0	MD:Z:83\n")
# 	f.write("HISEQ1:125:D0FV3ACXX:4:1205:5026:775181	163	chr1	177014	0	69M32S	=	177159	240	GGACATGGGAGAATGAAGGTGGTTGAATTGTTCATATTAAAGAACTTCCACCCAGATTGAAATAAAAGAGAGAAGAATAGAGAGGGCAGATCGAGATCCGA	::+44=BB:3+0AE<BEGAC3<)22C<*1*@?*:CBF9D**?*?:B99?4?((?88=B=)8))8C4CEH################################	RG:Z:KOP1277_cutadapt_long	XC:i:69	XT:A:R	NM:i:4	SM:i:0	AM:i:0	X0:i:10	X1:i:0	XM:i:4	XO:i:0	XG:i:0	MD:Z:8T18A31C2G6\n")
# 	f.write("HISEQ1:125:D0FV3ACXX:4:1205:5026:775181	83	chr1	177159	0	6S95M	=	177014	-240	CTGACAAAAATTAAAGACAGAAACCAAAGTTTAGCCTGAGACTACAATTAATTGGGCAATAAGCCAGAGGCACATATGGCATAAGACAGATTTAAACATTT	######EED?3@C=A;?EAAHC@AHCCFF=3BGBF?<BHDF@FGGDB4@F?BGD@@HGGAIIIIGCDH?<BH@HBHAGE?F9GHGF<EDDDADDDDDD@@?	RG:Z:KOP1277_cutadapt_long	XC:i:95	XT:A:R	NM:i:0	SM:i:0	AM:i:0	X0:i:6	X1:i:0	XM:i:0	XO:i:0	XG:i:0	MD:Z:95\n")
# 	f.write("HISEQ1:125:D0FV3ACXX:4:1205:5026:775182	163	chr1	177014	0	69M32S	=	177159	240	GGACATGGGAGAATGAAGGTGGTTGAATTGTTCATATTAAAGAACTTCCACCCAGATTGAAATAAAAGAGAGAAGAATAGAGAGGGCAGATCGAGATCCGA	::+44=BB:3+0AE<BEGAC3<)22C<*1*@?*:CBF9D**?*?:B99?4?((?88=B=)8))8C4CEH################################	RG:Z:KOP1277_cutadapt_long	XC:i:69	XT:A:R	NM:i:4	SM:i:0	AM:i:0	X0:i:10	X1:i:0	XM:i:4	XO:i:0	XG:i:0	MD:Z:8T18A31C2G6\n")
# 	f.write("HISEQ1:125:D0FV3ACXX:4:1205:5026:775182	83	chr1	177159	0	6S95M	=	177014	-240	CTGACAAAAATTAAAGACAGAAACCAAAGTTTAGCCTGAGACTACAATTAATTGGGCAATAAGCCAGAGGCACATATGGCATAAGACAGATTTAAACATTT	######EED?3@C=A;?EAAHC@AHCCFF=3BGBF?<BHDF@FGGDB4@F?BGD@@HGGAIIIIGCDH?<BH@HBHAGE?F9GHGF<EDDDADDDDDD@@?	RG:Z:KOP1277_cutadapt_long	XC:i:95	XT:A:R	NM:i:0	SM:i:0	AM:i:0	X0:i:6	X1:i:0	XM:i:0	XO:i:0	XG:i:0	MD:Z:95\n")
# 	f.write("HISEQ1:125:D0FV3ACXX:4:1205:5026:775183	163	chr1	177014	0	69M32S	=	177159	240	GGACATGGGAGAATGAAGGTGGTTGAATTGTTCATATTAAAGAACTTCCACCCAGATTGAAATAAAAGAGAGAAGAATAGAGAGGGCAGATCGAGATCCGA	::+44=BB:3+0AE<BEGAC3<)22C<*1*@?*:CBF9D**?*?:B99?4?((?88=B=)8))8C4CEH################################	RG:Z:KOP1277_cutadapt_long	XC:i:69	XT:A:R	NM:i:4	SM:i:0	AM:i:0	X0:i:10	X1:i:0	XM:i:4	XO:i:0	XG:i:0	MD:Z:8T18A31C2G6\n")
# 	f.write("HISEQ1:125:D0FV3ACXX:4:1205:5026:775183	83	chr1	177159	0	6S95M	=	177014	-240	CTGACAAAAATTAAAGACAGAAACCAAAGTTTAGCCTGAGACTACAATTAATTGGGCAATAAGCCAGAGGCACATATGGCATAAGACAGATTTAAACATTT	######EED?3@C=A;?EAAHC@AHCCFF=3BGBF?<BHDF@FGGDB4@F?BGD@@HGGAIIIIGCDH?<BH@HBHAGE?F9GHGF<EDDDADDDDDD@@?	RG:Z:KOP1277_cutadapt_long	XC:i:95	XT:A:R	NM:i:0	SM:i:0	AM:i:0	X0:i:6	X1:i:0	XM:i:0	XO:i:0	XG:i:0	MD:Z:95\n")
## uniqueify a list, from here: http://stackoverflow.com/questions/480214/how-do-you-remove-duplicates-from-a-list-in-python-whilst-preserving-order
def unique(seq):
    seen = set()
    seen_add = seen.add
    return [ x for x in seq if x not in seen and not seen_add(x)]
