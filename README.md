# README #

### What is mergeDuplicates 

MergeDuplicates is a tool merged PCR duplicates and boosts base qualities. 

### How do I get set up? 

1. Make sure samtools is in your $PATH
2. Clone this repo

```
#!bash

git clone https://dakl@bitbucket.org/dakl/mergeduplicates.git
cd mergeduplicates
python mergeduplicates.py [options]

```


### Options

```

Usage: mergeduplicates.py [options]

Options:
  -h, --help            show this help message and exit
  -i INPUT, --input=INPUT
                        Input BAM file to split
  -o OUTPUT, --output=OUTPUT
                        Output for BAM file
  -x METRICS, --metrics=METRICS
                        Output for metrics
  -M MAXQUAL, --maxqual=MAXQUAL
                        Maximum BQ after merging
  -m MINQUAL, --minqual=MINQUAL
                        Minimum BQ to cosider base for merging (do not change
                        unless you know what you are doing)
  -B, --use-best-qual   If set, picks the highest base quality instead of
                        adding the base qualities when merging duplicates
  -n NUMBEROFREADSBETWEENREPORTS, --numberofreadsbetweenreports=NUMBEROFREADSBETWEENREPORTS
                        Number of reads between progress reports

```
